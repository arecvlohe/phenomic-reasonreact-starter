# Phenomic ReasonReact Starter

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/arecvlohe/phenomic-reasonreact-starter.git)

## Summary

[Phenomic](https://phenomic.io/) with [ReasonReact](https://reasonml.github.io/reason-react/) using:

- [bs-css](https://github.com/SentiaAnalytics/bs-css) & [emotion](https://emotion.sh/) (for styling and ssr, respectively)
- custom webpack config (for additional loaders such as svg)

## Motivation

In order to use Phenomic with my choice of tools I need to customize the webpack config, which requires updating dependencies, which requires trial and error. To reduce the amount of trial and error I created this repository as a starter to get up and running and creating a static site using ReasonReact.
